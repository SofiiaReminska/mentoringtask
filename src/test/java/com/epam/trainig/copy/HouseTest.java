package com.epam.trainig.copy;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class HouseTest {

    @Test
    public void copyTest() throws CloneNotSupportedException {

        List<Furniture> furnitures = Arrays.
                asList(new Furniture("Sofa", 1),
                        new Furniture("Chair", 2),
                        new Furniture("Table", 1));

        List<Furniture> furnitures1 = Arrays.
                asList(new Furniture("Sofa", 2),
                        new Furniture("Chair", 4),
                        new Furniture("Table", 2));

        Room room = new Room(20, "LivingRoom", furnitures);
        Room room1 = new Room(18, "Kitchen", furnitures1);

        List<Room> rooms = Arrays.asList(room, room1);

        House house = new House();
        house.setRooms(rooms);
        assertEquals(house, house.clone());
        assertNotSame(house, house.clone());
    }
}
