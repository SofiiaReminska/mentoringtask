package com.epam.trainig.immutable;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        int i = 2;
        Integer i2 = 3;
        List<Character> list = Arrays.asList('s', 'u', 'n');

        OwnImmutableClass ownImmutableClass = new OwnImmutableClass(i, i2, list);

        System.out.println(i == ownImmutableClass.getNumber());
        System.out.println(i2 == ownImmutableClass.getNumber2());
        System.out.println(list == ownImmutableClass.getList());
        System.out.println(ownImmutableClass.getNumber());
        System.out.println(ownImmutableClass.getNumber2());
        System.out.println(ownImmutableClass.getList());

        i = 4;
        i2 = 5;
        list.set(0,'f');

        System.out.println(ownImmutableClass.getNumber());
        System.out.println(ownImmutableClass.getNumber2());
        System.out.println(ownImmutableClass.getList());
    }
}
