package com.epam.trainig.immutable;

import java.util.ArrayList;
import java.util.List;

public final class OwnImmutableClass {
    private final int number;
    private final Integer number2;
    private final List<Character> list;

    public OwnImmutableClass() {
        this(0, 0, new ArrayList<>());
    }

    public OwnImmutableClass(int n, Integer n2, List<Character> list) {
        this.number = n;
        this.number2 = n2;
        this.list = new ArrayList<>(list);
    }

    public List<Character> getList() {
        return new ArrayList<>(list);
    }

    public int getNumber() {
        return number;
    }

    public Integer getNumber2() {
        return number2;
    }
}
