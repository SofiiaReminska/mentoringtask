package com.epam.trainig.copy;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class House implements Cloneable {
    private List<Room> rooms;


    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        House house = (House) super.clone();
        List<Room> list = new ArrayList<>();
        for (Room el : house.rooms) {
            list.add((Room) el.clone());
        }
        house.rooms = list;
        return house;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof House)) return false;
        House house = (House) o;
        return Objects.equals(rooms, house.rooms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rooms);
    }
}
