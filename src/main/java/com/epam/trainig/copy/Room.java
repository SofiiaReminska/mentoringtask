package com.epam.trainig.copy;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Room implements Cloneable {
    private List<Furniture> furnitureList;
    private double area;
    private String name;

    public Room(double area, String name, List<Furniture> furnitures) {
        this.area = area;
        this.name = name;
        this.furnitureList = furnitures;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public List<Furniture> getFurnitureList() {
        return furnitureList;
    }

    public void setFurnitureList(List<Furniture> furnitureList) {
        this.furnitureList = furnitureList;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Room room = (Room) super.clone();
        List<Furniture> list = new ArrayList<>();
        for (Furniture el : room.furnitureList) {
            list.add((Furniture) el.clone());
        }
        room.furnitureList = list;
        return room;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;
        Room room = (Room) o;
        return room.name.equals(name) &&
                Double.compare(room.area, area) == 0 &&
                Objects.equals(furnitureList, room.furnitureList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(furnitureList, area, name);
    }
}
